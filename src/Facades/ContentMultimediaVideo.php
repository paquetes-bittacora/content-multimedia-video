<?php

namespace Bittacora\ContentMultimediaVideo\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimediaVideo\ContentMultimediaVideo
 */
class ContentMultimediaVideo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Bittacora\ContentMultimediaVideo\ContentMultimediaVideo::class;
    }
}
