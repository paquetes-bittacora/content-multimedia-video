<?php

namespace Bittacora\ContentMultimediaVideo;

use Bittacora\ContentMultimediaVideo\Http\Livewire\ContentMultimediaVideoWidget;
use Bittacora\ContentMultimediaVideo\Http\Livewire\ContentMultimediaVideoWidgetTable;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimediaVideo\Commands\ContentMultimediaVideoCommand;

class ContentMultimediaVideoServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia-video')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia-video_table')
            ->hasCommand(ContentMultimediaVideoCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia-video', function($app){
            return new ContentMultimediaVideo();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia-video');
        Livewire::component('content-multimedia-video::content-multimedia-video-widget', ContentMultimediaVideoWidget::class);
        Livewire::component('content-multimedia-video::content-multimedia-video-widget-table', ContentMultimediaVideoWidgetTable::class);
    }
}
