<?php

namespace Bittacora\ContentMultimediaVideo\Commands;

use Illuminate\Console\Command;

class ContentMultimediaVideoCommand extends Command
{
    public $signature = 'content-multimedia-video';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
