<?php

namespace Bittacora\ContentMultimediaVideo\Http\Livewire;

use App\Models\User;
use Bittacora\ContentMultimediaVideo\Models\ContentMultimediaVideoModel;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ContentMultimediaVideoWidgetTable extends DataTableComponent
{
    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public string $defaultReorderColumn = 'order_column';
    public bool $showPerPage = false;
    public bool $showPagination = false;
    public bool $showSearch = false;
    public array $perPageAccepted = [0];

    protected $listeners = ['refreshContentMultimediaVideoWidgetTable' => '$refresh'];

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make('Título', 'id')->view('content-multimedia-video::livewire.datatable.title-column'),
            Column::make('Nombre del archivo', 'id')->view('content-multimedia-video::livewire.datatable.filename-column'),
            Column::make('Activo', 'active')->view('content-multimedia-video::livewire.datatable.active-column'),
            Column::make('Orden', 'order_column')->view('content-multimedia-video::livewire.datatable.order-column'),
            Column::make('', 'id')->view('content-multimedia-video::livewire.datatable.media-detach')
        ];
    }

    public function builder(): Builder
    {
        return ContentMultimediaVideoModel::query()->select(['id', 'content_id', 'multimedia_id', 'order_column', 'active'])->where('content_id', $this->contentId)->orderBy('order_column', 'ASC');
    }

    public function reorder($list){
        foreach($list as $item){
            ContentMultimediaVideoModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }
}
