<?php

namespace Bittacora\ContentMultimediaVideo\Http\Livewire;

use Bittacora\ContentMultimediaVideo\Models\ContentMultimediaVideoModel;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class ContentMultimediaVideoWidget extends Component
{
    public int $contentId;
    public ?Collection $videos = null;

    protected $listeners = ['refreshWidget' => '$refresh'];

    public function mount()
    {
        $contentMultimediaData = ContentMultimediaVideoModel::where([
            ['content_id', '=', $this->contentId]
        ])->orderBy('order_column', 'ASC')->get();

        if (!empty($contentMultimediaData)) {
            foreach ($contentMultimediaData as $key => $content) {
                /**
                 * @var ContentMultimediaVideoModel $content
                 */
                $multimedia = Multimedia::where('id', $content->multimedia_id)->with('mediaModel')->first();
                $content->setAttribute('multimedia', $multimedia);

            }

            $this->videos = $contentMultimediaData;
        }
    }

    public function render()
    {
        return view('content-multimedia-video::livewire.content-multimedia-video-widget')->with([
            'videos' => $this->videos,
            'contentId' => $this->contentId
        ]);
    }
}
